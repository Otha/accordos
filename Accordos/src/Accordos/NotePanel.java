package Accordos;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;


public class NotePanel extends JPanel {
    private static final List<Integer> Xlist = Arrays.asList(0,45,128,200,283,367,442);
     BufferedImage img;
     String currentacc;
     
     public NotePanel(){
           currentacc = null;
          try {
             img = ImageIO.read(new File("guitar.png"));
              } catch (IOException ex) {
            Logger.getLogger(DrawAcc.class.getName()).log(Level.SEVERE, null, ex);
                }
         
          setPreferredSize(new Dimension(300,500));         
          
     }
     
     public void setCurracc(String s){currentacc = s;}
     
     @Override
     public void paintComponent(Graphics g){
         super.paintComponent(g);
         Graphics2D g2 = (Graphics2D) g;
         g.drawRect(0, 0, 60, 90);
         g.drawImage(img, 0, 0, this);
          g.setFont(new Font("TimesRoman", Font.BOLD, 20));
         if(currentacc != null){
            for(int i = 0;i <= currentacc.length()-1;i++){
                if(currentacc.charAt(i)!='X'){
                g2.fillOval(i*53,  Xlist.get(Character.getNumericValue(currentacc.charAt(i))), 30, 30);
                }
            }
         }
    }
}
