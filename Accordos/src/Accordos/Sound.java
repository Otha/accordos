package Accordos;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Synthesizer;


public class Sound {
    private Synthesizer guitar;
    private final MidiChannel chan;
    private static final List<Integer> liste = Arrays.asList(40,45,50,55,59,64);
    int vel  = 150;
    int chanNumber = 25;
    int dur = 200;

    public Sound(){
        try{
            guitar = MidiSystem.getSynthesizer();
            guitar.open();
        }
        catch(MidiUnavailableException e){
            Logger.getLogger(Sound.class.getName()).log(Level.SEVERE, null, e);
        }
        chan = guitar.getChannels()[0];
        chan.programChange(chanNumber);
    }
    public int vol = 100;

    public void playChords(String notes){
        for(int i =0 ; i< notes.length();i++){
            if(notes.charAt(i)!='X'){
                chan.noteOn(Character.getNumericValue(notes.charAt(i))+liste.get(i), vol);
                try {
                    Thread.sleep(vel);
                } 
                catch (InterruptedException e) {}
           
            }
        }
         try {
                    Thread.sleep(dur);
                } 
                catch (InterruptedException e) {}
        chan.allNotesOff();
    }
    public void setVelocity(int a){
        vel  = a;  
    }
    public void setDuration(int a){
        dur  = a;  
    }
    public void setChanNumber(int a){
        chan.programChange(a);
    }
    
}
