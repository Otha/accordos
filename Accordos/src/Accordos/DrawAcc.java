
package Accordos;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;





public class DrawAcc extends JFrame {

    

    JPanel currNotePanel,main;
    private static  Init inits ;
    JMenuItem chordsList,play,quit;
    JMenu menu, submenu, ssubmenu,settings;
    String currChord;
    JRadioButtonMenuItem rbMenuItem;
   
    JLabel currNote,speed,duration;
    JMenuBar menubar_ ;
    NotePanel notePanel;
    Sound sound;
    JComboBox cb;
    JSlider vel,dur;
    ImageIcon img,exit,imenu,iset,iplay;
    public   DrawAcc() throws IOException{
        
        super("Accordos");
        //init icon    
        img = new ImageIcon("ico.png");
        exit = new ImageIcon("exit.png");
        imenu = new ImageIcon("menu.png");
        iset = new ImageIcon("settings.png");
        iplay = new ImageIcon("play.png");
            


            
        //init all Jpanel    
        main = new JPanel();
        currNotePanel = new JPanel();
        notePanel = new NotePanel();

        sound = new Sound();
        inits =  new Init("Accord.txt");
        List<String> note = inits.GetNote();
        //init all menu component
        menubar_ = new JMenuBar();
            
        menu = new JMenu("Chord");
        submenu = new JMenu("Chords List");
        settings = new JMenu();
            
        play = new JMenuItem("",iplay);
        chordsList = new JMenuItem();
        quit = new JMenuItem("",exit);
            
        currNote = new JLabel("E               A               D               G               B               E");
        speed = new JLabel("Speed");
        duration = new JLabel("Sustain");


            
        vel = new JSlider(JSlider.HORIZONTAL,0, 600, 300);
        dur = new JSlider(JSlider.HORIZONTAL,0, 800, 400);
        String[] guitarType = { "classic", "folk", "electric1", "electric2", "orverdrive","distortion" };
        cb = new JComboBox(guitarType);
        cb.setSelectedIndex(0);
               
            
        //set     
        menu.setIcon(imenu);
        settings.setIcon(iset);
        play.setPreferredSize(new Dimension(16,16));
        quit.setPreferredSize(new Dimension(16,16));
        currChord = ("000000");

        for(int i=0; i< note.size();i++){
            JMenuItem a = new JMenuItem(note.get(i));
            a.addActionListener((ActionEvent e) -> {
                setTitle("Accordos - "+a.getText());
                currChord = inits.getAcc(a.getText());
                notePanel.setCurracc(currChord);
                notePanel.repaint();
                currNote.setText(inits.getCurrNote( inits.getAcc(a.getText())));
                repaint();
            });                
            if(a.getText().length()==1){
                ssubmenu  = new JMenu(a.getText());
                submenu.add(ssubmenu);
            }
            ssubmenu.add(a);
        }
            
        

        quit.addActionListener((ActionEvent e) -> {
            System.exit(0);
        });
            
        play.addActionListener((ActionEvent e) -> {
            sound.playChords(currChord);
        });
        
            
        vel.addChangeListener((ChangeEvent e) -> {
            sound.setVelocity(vel.getValue());
        });
        dur.addChangeListener((ChangeEvent e) -> {
            sound.setDuration(dur.getValue());
        });

      
       cb.addActionListener((ActionEvent e) -> {
           sound.setChanNumber(cb.getSelectedIndex()+25);
        });
       
        
        //Shortcut

        menu.setMnemonic(KeyEvent.VK_A);
        quit.setMnemonic(KeyEvent.VK_Q);
        submenu.setMnemonic(KeyEvent.VK_S);
        
        settings.add(speed);
        settings.add(vel);
        settings.add(duration);
        settings.add(dur);
        menu.add(submenu);
        menubar_.add(play);
        menubar_.add(menu);
        menubar_.add(cb);
        menubar_.add(settings);
        menubar_.add(quit);
        currNotePanel.add(currNote);
            
            
        main.add(currNotePanel);
        main.add(notePanel);
    }
    void setDrawAcc( ){
            setIconImage(img.getImage());
            setContentPane(main);
            setSize(300,580);
            setVisible(true);
            setResizable(false);
            setJMenuBar(menubar_);
            addWindowListener (new WindowAdapter(){
			public void windowClosing (WindowEvent e){
				System.exit(0);
			}
		});
        
    }
}
    
    
 

