package Accordos;

import java.io.FileReader ;
import java.util.Map;
import java.io.BufferedReader ;
import java.io.IOException ;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.List;
import java.util.TreeMap;

/**
 *
 * @author Otha 
 */


public class Init {
       private final Map<String, String> accords;
       private final Integer number;
       private static Pattern p;
       private static Matcher matcher;
       private static final List<String> liste = Arrays.asList("C","C#","D","D#","E","F","F#","G","G#","A","A#","B");
       public Init(String path) throws IOException{
        this.accords = new TreeMap<>();
           BufferedReader buffer;
           String tmp;
       
           try{
               buffer = new BufferedReader(new FileReader(path));
               while (buffer.ready()==true){
                    tmp = buffer.readLine();
                    if (Pattern.matches("(^[A-G](#|b)?m?(7|4)?):([0-6-X]{6})",tmp)){
                        String[] parts = tmp.split(":");
                        accords.put(parts[0],parts[1]);
                    }
               }
           }
           catch (IOException e){
               System.out.println("Error IO");
           }
           number = accords.size();
         
       }
         public List GetNote(){
          List<String> note = new ArrayList();
          accords.keySet().forEach((key) -> {
              note.add(key);
           });
          return note;
         }

       public String getAcc(String i){
           return accords.get(i);
       }
       
       //get each note played 
       public String getCurrNote(String str){
           String a = new String();
           String b = new String();
           List<Integer> not = Arrays.asList(4,9,2,7,11,4);
           for(int i =0;i<=5;i++){
               a="";
               if(str.charAt(i)=='X'){
                   a+= "X";
               }
               else{
                 a += liste.get((not.get(i)+Character.getNumericValue(str.charAt(i)))%11);
               }
                 if(i<=4){
                 while(a.length()<15)
                 a += " ";
                 }
                 b +=a;
                 
           }
           return b;
       }
       

 }

       
